# pp-dask


## Interactive Data Analysis with Dask 
  This repository aims to make it easy for users to perform exploratory data analysis and visualization
  remotely on [Virgo](https://hpc.gsi.de/virgo) with  [jupyter](https://jupyter.org/) 
  and [python-dask](https://www.dask.org/)
   
## Getting started 

- [ ] Open a first terminal (**A**) and login to bare-metal submit node on [virgo2](https://hpc.gsi.de/virgo)
```
ssh username@virgo2.hpc.gsi.de
```

- [ ] Create and move to your dask working directory on your `/lustre` partition. This will define the location where all your jupyter notebook will be stored.

```
cd /lustre/<group>/<username>
mkdir -p work_dask
cd work_dask
```

- [ ] Execute the dedicated dask singularity image:

```
export DASK_CONT=/cvmfs/phelix.gsi.de/sifs/cpu/dev/rlx8_ompi_ucx_dask.sif 
singularity exec -B /usr/lib64/slurm  -B /etc/slurm -B /var/run/munge -B /var/spool/slurm   -B /var/lib/sss/pipes -B /cvmfs -B /lustre  ${DASK_CONT}  bash -l'
```
This command being too long, make it an alias and add it to you linux profile (`.bashrc`):
```
export DASK_CONT=/cvmfs/phelix.gsi.de/sifs/cpu/dev/rlx8_ompi_ucx_dask.sif 
alias pp-dask='singularity exec -B /usr/lib64/slurm  -B /etc/slurm -B /var/run/munge -B /var/spool/slurm   -B /var/lib/sss/pipes -B /cvmfs -B /lustre  ${DASK_CONT}  bash -l'
```

- [ ] If everything goes well you should see such a prompt:

```
[dbertini@lxbk1130 /lustre/rz/dbertini/work_dask]$ pp-dask
 
RLX system profile loaded ...
[pp_dask]/lustre/rz/dbertini/work_dask
```

- [ ] Start Jupyter from within the container in headless mode and choosing the port

```
jupyter lab --no-browser --port 8080

```

- [ ] Jupyter will show you a `127.0.0.1` (aka `localhost`) URL, on choosen port TCP 8080 and a
generated token associated to the jupyter session


- [ ] At this level, you cannot directly reach that URL with your local browser. You will need to start another terminal (**B**) and connect the same submit node using SSH port tunneling (port forwarding):
So concrete: if you logged in `lxbk1131` then

```
ssh -L 8080:localhost:8080 dbertini@lxbk1131
```

Here `lxbk1131` is just an alias to be able to directly log in this node. To create the alias on can
add an entry in the `$HOME/.ssh/config` as:

```
Host lxpool
  User dbertini
  ForwardAgent no
  Hostname lx-pool.gsi.de

Host lxbk1130
  User dbertini
  Hostname lxbk1130.gsi.de
  ProxyJump lxpool

```
Notes:
 - Jupyter on the desktop will increase the port if already in use.
 - take another port on your local desktop/laptop if you have local Jupyter instances still running

- [ ] Open the browser on your local desktop/laptop, open the URL from Jupyter with

```
http://localhost:8080/<token>

```
or equivalently

```
 http://127.0.0.1:8080/<token>

```
in it.

- [ ] To close connection:
    - stop Jupyter in terminal (**A**) : `Ctrl+C` and confirm with `y`, `Enter`
    - `Ctrl+C` the SSH tunnel in terminal (**B**)

## Dedicated interactive container
The interactive container features latest version of standard data analysis libraries and framework:

- [Python](https://www.python.org/) v3.12 
- [Jupyter](https://jupyter.org/) ecosystem.
- [Dask](https://www.dask.org/) complete ecosystem.
- [openPMD](https://openpmd-api.readthedocs.io/en/0.15.2/)
- [PyTorch](https://pytorch.org/): high-productivity Deep Learning framework based on dynamic computation graphs and automatic differentiation. Can be used to replace a computing intensive plasma simulation with a
[neural network surrogate model](https://warpx.readthedocs.io/en/latest/usage/workflows/ml_dataset_training.html#training-a-surrogate-model-from-warpx-data)
- [SLURM](https://slurm.schedmd.com/documentation.html) commands. Needed for the [Dask distributed](https://distributed.dask.org/en/stable/) module. 

Singularity definition file is available from this repository:
- `/defs/rlx8_ompi_ucx_dask.def`

## Available notebooks
Example how to perform data analysis remotely are available from this repository.

- `/notebooks/open_viewer_test.ipynb` : Example showing how to use [openviewer](https://github.com/openPMD/openPMD-viewer/tree/dev)
- `/notebooks/dask_opmd.ipynb`: Example of Data analysis using [openpmd-api](https://openpmd-api.readthedocs.io/en/0.15.2/)
and [Dask](https://www.dask.org/)



